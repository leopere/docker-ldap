# Lazy way to LDAP
Current state=**BROKEN**

The goal of this repository  is to produce a working LDAP setup that can have a
web panel "phpLdapAdmin" fired up on demand and destroyed when not needed.  
Eventually whenever the creator of the source containers impliments the --link
function we will be able to avoid exposing any ports as an option for increased
security.

This repo can eventually be forked at some point to contain actual private data
including the database/ and config/ directories.

## Side Note
This really doesnt completely follow the docker mentality of basically "Container
everything" as it makes host FS mounts.  Since its somewhat easier for some to
just use external file hosts like gitlab.com and github.com.  To store their data
securely and externally.

It could very easily be made to use a data volume container as well.  One day
if I'm not feeling lazy which is unlikely.  I will create a container script
that adheres to this standard but today will not be that day.

## Source

    https://github.com/osixia/docker-openldap
    https://github.com/osixia/docker-phpLDAPadmin

## Usage

    git clone https://gitlab.com/chamunks/docker-ldap.git
    cd docker-ldap/
    ./ldap.sh help ## For usage.  You will need to configure this to your needs.
    ./ldap.sh initialize # This initializes your databases and then starts the main app.

## Last thing to remember
I'm very lazy so its highly likely that this readme.md will be out of sync with
the feature-set in this script so you will have to read the script.  I'm making
a strong effort to keep it highly readable and simplistic.
