#!/bin/bash
## One day I may consider making this an external config file.
## Who knows.
ORGANIZATION=ldap.nixc.us
DOMAIN=ldap.nixc.us
PASSWORD=EdwardSn0wden
ROOT=$(pwd)
CONTAINER=nixius
EXPOSEPLAIN=389
EXPOSESSL=636
PANELPORT=8585

case $1 in
	start )
		docker start $CONTAINER-ldap
	;;
	stop )
		docker stop $CONTAINER-ldap
	;;
	destroy )
		docker stop $CONTAINER-ldap
		docker rm -f $CONTAINER-ldap
	;;
	initialize )
		docker run --name $CONTAINER-ldap -p $EXPOSEPLAIN:389 -e LDAP_ORGANISATION='"$ORGANIZATION"' -e LDAP_DOMAIN='"$DOMAIN"' -e LDAP_ADMIN_PASSWORD='"$PASSWORD"' -v $ROOT/database:/var/lib/ldap -v $ROOT/config:/etc/ldap/slapd.d -d chamunks/openldap
	;;
	create )
		# Because this guy does some weird crap with if statements and init scripts to make it more magical
		# I need to probably make an initialize function as well for people using this to create it for the first time.
		docker run --name $CONTAINER-ldap -p $EXPOSEPLAIN:389 -e LDAP_ORGANISATION='"$ORGANIZATION"' -e LDAP_DOMAIN='"$DOMAIN"' -e LDAP_ADMIN_PASSWORD='"$PASSWORD"' -v $ROOT/database:/var/lib/ldap -v $ROOT/config:/etc/ldap/slapd.d -v $ROOT/.placeholder:/etc/docker-openldap-first-start-done -d chamunks/openldap
	;;
	getip )
		# Fix this the var $CONTAINER is borked.
		CONTAINERIP=$(docker inspect --format='{{.NetworkSettings.IPAddress}}' $CONTAINER)
		echo 'If your container is running your docker IP will be below this line:'
		echo $CONTAINERIP
	;;
	start-panel )
		#CONTAINERIP=$(docker inspect --format='{{.NetworkSettings.IPAddress}}' $CONTAINER)
		echo 'Booting a fresh panel with a new ssl key.'
		#docker run --name $CONTAINER-phpldapadmin -e LDAP_HOSTS=$DOMAIN -e SERVER_NAME=$DOMAIN -p $PANELPORT:443 -d osixia/phpldapadmin
		docker run --name $CONTAINER-phpldapadmin -e LDAP_HOSTS=$DOMAIN -p $PANELPORT:443 -d osixia/phpldapadmin
	;;
	stop-panel )
		echo 'Destroying old panel.'
		docker rm -f $CONTAINER-phpldapadmin
	;;
	* )
		echo 'Usage:    ./ldap.sh {start|stop|destroy|initialize|create|getip}'
		echo '---------------------------------------------------------------------'
		echo 'start:    Starts the ldap docker container.'
		echo 'stop:     Stops the ldap docker container.'
		echo 'destroy:  This only destroys the appliance meaning your data should be'
		echo '          safe and secure in $ROOT/data'
		echo 'initialize:   This installs and creates a freshly configured ldap container.'
		echo 'create:   This installs and creates a freshly configured ldap container.'
		echo 'getip:    If the container is currently running this gets its IP.'
		echo '---------------------------------------------------------------------'
		echo '---------------------------------------------------------------------'
		echo 'start-panel	starts web-admin panel on configured port  '
		echo 'stop-panel	destroys web-admin panel'
		echo '---------------------------------------------------------------------'
	;;
esac
